class RPNCalculator < Array
  
  def initialize
    @value = 0
  end
  
  def plus
    self.length_checker
    @value = self.pop + self.pop
    self.push(@value)
  end
  
  def minus
    self.length_checker
    @value = -self.pop + self.pop
    self.push(@value)
  end
  
  def times
    self.length_checker
    @value = self.pop * self.pop
    self.push(@value)
  end
  
  def divide
    self.length_checker
    self.map! {|num| num.to_f}
    val_1 = self.pop
    val_2 = self.pop
    @value = val_2 / val_1
    self.push(@value)
  end
  
  def tokens(str)
    numbers = %w{ 1 2 3 4 5 6 7 8 9 0 }
    str.split.map do |num|
      if numbers.include?(num)
        num.to_i
      else
        num.to_sym
      end
    end
  end
  
  def evaluate(str)
    arr = tokens(str)
    arr.each do |el|
      case el
      when Fixnum
        self.push(el)
      when :+
        self.plus
      when :-
        self.minus
      when :*
        self.times
      when :\
        self.divide
      end
    end
    self.pop
  end
  
  def value
    @value
  end
  
  def length_checker
    if self.count <= 1
      raise "calculator is empty"
    end
  end
end
